import io
import string

import nltk
from idna import unicode
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn import tree
import pandas as pd

import service
import utils
import repository
from models import Dataset
import nltk
from sklearn.feature_extraction.text import TfidfTransformer, CountVectorizer

nltk.download('stopwords')
from string import punctuation
from sklearn import metrics



def remove_punctuation(textos):
    token_espaco = nltk.tokenize.WhitespaceTokenizer()
    token_pontuacao = nltk.tokenize.WordPunctTokenizer()
    token_espaco.tokenize()
    pontuacao = list()
    for ponto in punctuation:
        pontuacao.append(ponto)


def remove_stop_words(textos):
    descricoes = textos.description
    token_espaco = nltk.tokenize.WhitespaceTokenizer()
    palavras_irrelevantes = nltk.corpus.stopwords.words('portuguese')
    palavras_sem_acento = [unicode.unicode(texto) for texto in descricoes]
    frase_processada = list()
    for frase in descricoes:
        frase = frase.lower()
        nova_frase = list()
        palavras_texto = token_espaco.tokenize(frase)
        for palavra in palavras_texto:
            if palavra not in palavras_irrelevantes:
                nova_frase.append(palavra)
        frase_processada.append(' '.join(nova_frase))

    textos['processado'] = frase_processada
    return textos


def test_bow():
    import pandas as pd
    import numpy as np
    import nltk
    nltk.download('stopwords')
    from nltk import tokenize
    from sklearn.feature_extraction.text import CountVectorizer

    frase_processada = list()

    # palavras_irrelevantes = nltk.corpus.stopwords.words["portuguese"]
    palavras_irrelevantes = nltk.corpus.stopwords.words('portuguese')
    # print(palavras_irrelevantes)
    # Abrir arquivo e separar colunas
    resenha = pd.read_csv("datasets/dataset_anonimizada.csv", sep=';')
    processado = remove_stop_words(resenha)

    print('quantidade :',processado.processado.shape())
    # print(frases_processado.head())

    # print(resenha_sem_stopwords.head())

    # Vetorizar frases
    vectorizar = CountVectorizer()  # (max_features=100)
    bow = vectorizar.fit_transform(processado.processado)
    vectorizar.get_feature_names()  # (lowercase=false)

    # Mostrar em matriz
    matriz_esparsa = pd.DataFrame.sparse.from_spmatrix(bow,
                                                       columns=vectorizar.get_feature_names())

    # mapear coluna priority para valores numericos

    # separar treino e teste
    x = matriz_esparsa
    y = processado.priority
    treino, teste, classe_treino, classe_teste = train_test_split(x, y,
                                                                  random_state=65,
                                                                  stratify=y)
    # aplicando modelo de regressao logistica
    rl = LogisticRegression()
    previsao = rl.fit(treino, classe_treino).predict(teste)
    acuracia = rl.score(teste, classe_teste)

    print('----------------------------------------------BOW')
    print('')
    print('regressao logistica :')
    print('Pontuacao :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))
    print('')

    rnd_clf = RandomForestClassifier()
    previsao = rnd_clf.fit(treino, classe_treino).predict(teste)
    acuracia = rnd_clf.score(teste, classe_teste)
    print('florestas aleatorias :')
    print('Pontuacao :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))

    svm_clf = SVC()
    previsao = svm_clf.fit(treino, classe_treino).predict(teste)
    acuracia = svm_clf.score(teste, classe_teste)
    print('----------------------------------------------BOW')
    print('')
    print('SVM')
    print('Pontuacao :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))

    knn_clf = KNeighborsClassifier(n_neighbors=13)
    previsao = knn_clf.fit(treino, classe_treino).predict(teste)
    acuracia = knn_clf.score(teste, classe_teste)
    print('----------------------------------------------BOW')
    print('')
    print('KNN')
    print('Pontuacao :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))

    tree_clf = tree.DecisionTreeClassifier()
    previsao = tree_clf.fit(treino, classe_treino).predict(teste)
    acuracia = tree_clf.score(teste, classe_teste)
    print('----------------------------------------------BOW')
    print('')
    print('Tree')
    print('Pontuacao :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))

    gnb_clf = MultinomialNB().fit(treino, classe_treino)
    previsao = gnb_clf.predict(teste)
    acuracia = gnb_clf.score(teste, classe_teste)
    print('----------------------------------------------')
    print('')
    print('Naive Bayes :')
    print()
    print('Pontuacao :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))

    vc1 = VotingClassifier(
        estimators=([('rl', rl), ('svm_clf', svm_clf), ('knn_clf', knn_clf), ('tree_clf', tree_clf), ('gnb_clf', gnb_clf) ]),
        voting='hard')
    previsao = vc1.fit(treino, classe_treino).predict(teste)
    acuracia = vc1.score(teste, classe_teste)
    print('----------------------------------------------BOW')
    print('')
    print('Voting Acuracia :')
    print('Acuracia :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))

    vc2 = VotingClassifier(
        estimators=(
        [ ('tree_clf', tree_clf), ('gnb_clf', gnb_clf), ('rnd_clf', rnd_clf)]),
        voting='hard')
    previsao = vc2.fit(treino, classe_treino).predict(teste)
    acuracia = vc2.score(teste, classe_teste)
    print('----------------------------------------------BOW')
    print('')
    print('Voting2 Acuracia :')
    print('Acuracia :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))


def test_tf_idf():
    import pandas as pd
    import numpy as np
    import nltk
    nltk.download('stopwords')
    from nltk import tokenize
    from sklearn.feature_extraction.text import CountVectorizer

    frase_processada = list()

    # palavras_irrelevantes = nltk.corpus.stopwords.words["portuguese"]
    palavras_irrelevantes = nltk.corpus.stopwords.words('portuguese')
    # print(palavras_irrelevantes)
    # Abrir arquivo e separar colunas
    resenha = pd.read_csv("datasets/dataset_anonimizada.csv", sep=';')
    #processado = remove_stop_words(resenha)
    # print(frases_processado.head())

    # print(resenha_sem_stopwords.head())
    print('shape :',resenha.priority.shape);
    d_x = resenha.priority.shape[0];
    print ('dimensões',d_x)

    # Vetorizar frases
    vectorizar = CountVectorizer()  # (max_features=100)
    tf_idf = vectorizar.fit_transform(resenha.priority)

    print(tf_idf.shape)
    # separar treino e teste
    x = tf_idf
    y = processado.priority
    treino, teste, classe_treino, classe_teste = train_test_split(x, y,
                                                                  random_state=65,
                                                                  stratify=y)
    # aplicando modelo de regressao logistica
    rl = LogisticRegression()
    previsao = rl.fit(treino, classe_treino).predict(teste)
    acuracia = rl.score(teste, classe_teste)

    print('----------------------------------------------TF-IDF')
    print('')
    print('regressao logistica :')
    print('Pontuacao :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))
    print('')

    rnd_clf = RandomForestClassifier()
    previsao = rnd_clf.fit(treino, classe_treino).predict(teste)
    acuracia = rnd_clf.score(teste, classe_teste)
    print('florestas aleatorias :')
    print('Pontuacao :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))

    svm_clf = SVC()
    previsao = svm_clf.fit(treino, classe_treino).predict(teste)
    acuracia = svm_clf.score(teste, classe_teste)
    print('----------------------------------------------TF-IDF')
    print('')
    print('SVM')
    print('Pontuacao :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))

    knn_clf = KNeighborsClassifier(n_neighbors=13)
    previsao = knn_clf.fit(treino, classe_treino).predict(teste)
    acuracia = knn_clf.score(teste, classe_teste)
    print('----------------------------------------------TF-IDF')
    print('')
    print('KNN')
    print('Pontuacao :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))

    tree_clf = tree.DecisionTreeClassifier()
    previsao = tree_clf.fit(treino, classe_treino).predict(teste)
    acuracia = tree_clf.score(teste, classe_teste)
    print('----------------------------------------------TF-IDF')
    print('')
    print('Tree')
    print('Pontuacao :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))

    gnb_clf = MultinomialNB().fit(teste, classe_teste)
    previsao = gnb_clf.predict(teste)
    acuracia = gnb_clf.score(teste, classe_teste)
    print('----------------------------------------------TF-IDF')
    print('')
    print('Naive Bayes :')
    print()
    print('Pontuacao :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))

    vc1 = VotingClassifier(
        estimators=(
        [('rl', rl), ('svm_clf', svm_clf), ('knn_clf', knn_clf), ('tree_clf', tree_clf), ('gnb_clf', gnb_clf)]),
        voting='hard')
    previsao = vc1.fit(treino, classe_treino).predict(teste)
    acuracia = vc1.score(teste, classe_teste)
    print('----------------------------------------------TF-IDF')
    print('')
    print('Voting Acuracia :')
    print('Acuracia :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))

    vc2 = VotingClassifier(
        estimators=(
            [('tree_clf', tree_clf), ('gnb_clf', gnb_clf), ('rnd_clf', rnd_clf)]),
        voting='hard')
    previsao = vc2.fit(treino, classe_treino).predict(teste)
    acuracia = vc2.score(teste, classe_teste)
    print('----------------------------------------------TF-IDF')
    print('')
    print('Voting2 Acuracia :')
    print('Acuracia :', acuracia)
    print('F1 score    :', metrics.f1_score(classe_teste, previsao, average='macro'))
    print('Acurácia    :', metrics.accuracy_score(classe_teste, previsao))
    print('Precisão    :', metrics.precision_score(classe_teste, previsao, average='macro'))
    print('Revocação   :', metrics.recall_score(classe_teste, previsao, average='weighted'))
    print('Erro máximo :', metrics.max_error(classe_teste, previsao))


    print('Conjunto de treino e testeusado :')
    print('---------------------------------')
    print(treino,classe_treino)

def test_file_validation():
    print(utils.file_validation('datasets/dataset_anonimizada.csv', ';'))


def test_environment():
    repository.select()


def test_insert():
    path = 'datasets/dataset_anonimizada.csv'
    separator = ';'
    name = 'email_suport3.csv'
    file = repository.convertFileToBinary(path);
    repository.save_dataset(file, name, separator)


def test_select():
    result = repository.select_by_name('teste.csv')
    print(result)


def test_dao_dataset():
    dao = repository.DatasetRepository()


def test_select_all():
    repository.get_list_dataset()


def test_nltk():
    from nltk import tokenize
    # tokenização

    frase = "Bem vindo ao mundo do pln!"
    token_espaco = tokenize.WhitespaceTokenizer()
    token_fase = token_espaco.tokenize(frase)
    print(token_fase)

    frequencia = nltk.FreqDist(token_fase)
    print(frequencia)
    palavras_irrelevantes = nltk.corpus.stopwords.words["portuguese"]

'''
def test_tf_idf():
    import pandas as pd

    from sklearn.feature_extraction.text import TfidfVectorizer
    resenha = pd.read_csv("datasets/dataset_anonimizada.csv", sep=';')
    vect = TfidfVectorizer(ngram_range=(1, 4), use_idf=True, lowercase=True, min_df=2, max_df=0.95)
    # vect.fit(df.text_pt)
    # text_vect = vect.transform(df.text_pt)
'''

def test_update_status():
    print(repository.update_training_status(1, 'COMPLETED'))


def test_get_training():
    tupla = repository.get_training_status_by_id_dataset(1)
    print(tupla)


def test_get_dataset_by_id():
    tupla = repository.get_dataset_by_id(1)
    csv = tupla[1]
    name_file = tupla[2]
    sep = tupla[3]
    data_string = csv.decode('UTF-8')
    data = io.StringIO(data_string)
    df = pd.read_csv(data, sep=sep)

    print('separador:', sep)
    print('nome:', name_file)
    print(df.head())
    # print(csv)

def amostras():
    resenha = pd.read_csv("datasets/dataset_anonimizada.csv", sep=';')
    processado = remove_stop_words(resenha)
    vectorizar = CountVectorizer()  # (max_features=100)
    bow = vectorizar.fit_transform(processado.processado)
    vectorizar.get_feature_names()  # (lowercase=false)

    # Mostrar em matriz
    matriz_esparsa = pd.DataFrame.sparse.from_spmatrix(bow,
                                                       columns=vectorizar.get_feature_names())
    x = bow
    y = processado.priority
    treino_x, teste_x, treino_y, teste_y = train_test_split(x, y,
                                                                  random_state=65,
                                                                  test_size=0.30,
                                                                  stratify=y)


    print('Separados para treino')
    print(treino_y.value_counts())
    print('Separados para Teste')
    print(teste_y.value_counts())


def test_get_dataset_by_id():
    #retorno de dataset inexistente

    dataset, error = service.get_dataset(14)
    if (dataset):
        df = pd.DataFrame(dataset.file_content)
        csv = df.to_csv(sep=";" )
        linhas = csv.split("\n")
        novas_linhas =[]
        for i in range(1, len(linhas), 1):
            if len(linhas[i])>0 : novas_linhas.append(linhas[i].split("b'")[1].replace("\\n",'').replace("b\\","").replace('"',"").replace("'",""))

        nomes = novas_linhas[0].split(";")
        content = novas_linhas[1:]
        c0=[]
        c1=[]
        c2=[]
        c3=[]
        c4=[]
        for linhas in content:
            if len(linhas)>0 :
                if len(linhas.split(";")[0])>0:
                    c0.append(linhas.split(";")[0])
                else:
                    c0.append(0)
                if len(linhas.split(";")[1])>0:
                    c1.append(linhas.split(";")[1])
                else:
                    c1.append(0)
                if len(linhas.split(";")[2])> 0:
                    c2.append(linhas.split(";")[2])
                else:
                    c2.append(0)
                if len(linhas.split(";")[3]) > 0:
                    c3.append(linhas.split(";")[3])
                else:
                    c3.append(0)
                if len(linhas.split(";")[4]) > 0:
                    c4.append(linhas.split(";")[4])
                else:
                    c4.append(0)


        #print (c0)
        #print(c1)
        #print(c2)
        #print(c3)
        #print(c4)
        d = {nomes[0]: c0, nomes[1]: c1, nomes[2]:c2 , nomes[3]:c3, nomes[4]:c4}
        #print(d)
        df = pd.DataFrame(data=d)
        #print(df.head())
        result =list()
        descricoes = df.description
        for phrase in descricoes:
            result.append(
                phrase.replace("\\xc3\\xa7", "ç").replace("\\xc3\\xba", "u").replace("\\xc3\\xa1", "a").replace(
                    "\\xc3\\xaa",
                    "e").replace(
                    "\\xc3\\xa3", "a").replace("\\xc3\\xa9", "e").replace("\\xc3\\xb5", "o").replace("\\xc3\\xad",
                                                                                                     "í").replace(
                    "\\xc3\\xa0", "a").replace("\\xc3\\xb3", "o").replace("\\xc3\\x83", "a").replace("\\xc3\\xb4", "o"))


        df['description'] =result
        vectorizar = CountVectorizer(max_features=20)  # (max_features=100)
        bow = vectorizar.fit_transform(df.description)
        x = bow
        #y = new_content.priority
        #treino, teste, classe_treino, classe_teste = train_test_split(x, y,random_state=65,stratify=y)
        #print(bow)
        print(bow.shape[0])
        print(bow)
        #palavras_sem_acentos = [unidecode.unidecode(texto) for texto in textos]
        #stopwords_sem_acentos = [unidecode.unidecode(texto) for texto in stopwords]

    else:
        if error:
            print(error[0], error[1])

def pontuacao():
    print(string.punctuation)
    s = "string. With. Punctuation?"  # Sample string
    exclude = set(string.punctuation)
    s = ''.join(ch for ch in s if ch not in exclude)
    print(s)


def get_train_by_id_dataset():
    tuple = repository.get_training_status_by_id_dataset(45)
    print ()


def get_status_id():
    tuple = repository.get_training_status_by_id(41)
    if (tuple) :print(tuple[0],tuple[1],tuple[2])


def vetorizar_frase():
    message ="Ocorreu um problema com minha compra. Quero desistir e receber meu dinheiro de volta"
    remove_stop_words([message])




test_tf_idf()

#get_status_id()
#get_train_by_id_dataset
#test_get_dataset_by_id()

# test_file_validation()
# test_environment()
# test_insert()
# test_select_all()

# test_nltk()
# test_pandas()
# test_update_status()
# test_get_training()
#test_get_dataset_by_id()
#test_bow()
#test_tf_idf()
#amostras()
#pontuacao()