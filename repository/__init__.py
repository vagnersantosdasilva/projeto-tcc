import mysql.connector
import mysql.connector.cursor
import mysql.connector.errors
import os
import exceptions
from models import Dataset, Training

SQL_SAVE_DATASET = 'insert into dataset (name, file_content, limit_col) values(%s, %s, %s)'
SQL_UPDATE_DATASET = 'update dataset set name=%s , file_content = %s , limit_col =%s where id = %s'
SQL_FIND_DATASET_BY_NAME = 'select * from dataset where name = %s'
SQL_FIND_ALL_DATASET = 'select * from dataset'

SQL_SAVE_STATUS = 'insert into dataset (name, file_content, limit_col) values(%s, %s, %s)'
SQL_UPDATE_STATUS = 'update dataset set name=%s , file_content = %s , limit_col =%s where id = %s'
SQL_FIND_STATUS_BY_ID_TRAINING = 'select * from dataset where name = %s'


def getConnection():
    # TODO:disponibilizar conexao para quem precisar, verificar se já existe conexao aberta antes de solicitar nova

    local = os.environ.get('DB_SERVER')
    login = os.environ.get('DB_USER')
    password = os.environ.get('DB_PASSWORD')
    database = os.environ.get('DB_NAME')
    mydb = mysql.connector.connect(
        user=login,
        password=password,
        host=local,
        database=database
    )
    return mydb


def get_list_dataset():
    mydb = getConnection()
    mycursor = mydb.cursor()
    mycursor.execute(SQL_FIND_ALL_DATASET)
    tupla = mycursor.fetchall()
    list = make_list_dataset(tupla)
    mydb.close()
    return list


def make_list_dataset(rows):
    def convert(tupla):
        return Dataset(tupla[2], tupla[1], tupla[3], id=tupla[0])

    return list(map(convert, rows))


def get_dataset_by_id(id):
    select_query = "select * from dataset where id = %s"
    mydb = getConnection()
    mycursor = mydb.cursor()
    mycursor.execute(select_query, (id,))
    tupla = mycursor.fetchone()
    mydb.close()
    return tupla


def convertFileToBinary(filename):
    with open(filename, 'rb') as file:
        binaryData = file.read()
    return binaryData


def save_dataset(data_frame, name, separator):
    try:
        query = """insert into dataset (name, file_content, limit_col) values(%s, %s, %s)"""
        connection = getConnection()
        cursor = connection.cursor()
        blob_tuple = (name, data_frame, separator)
        cursor.execute(query, blob_tuple)
        id = cursor.lastrowid
        connection.commit()
        cursor.close()
        connection.close()
        return id
    except Exception as error:
        exceptions.handler_error(error)


def save_training_status(id_dataset, status):
    query = """insert into training_status (id_dataset, status ) values (%s, %s)"""
    connection = getConnection()
    cursor = connection.cursor()
    insert_tuple = (id_dataset, status)
    cursor.execute(query, insert_tuple)
    id_training_status = cursor.lastrowid
    connection.commit()
    cursor.close()
    connection.close()
    return Training(id_dataset, status, id_training_status)


def update_training_status(training_status):
    print(training_status.status, training_status.f1_score, training_status.precision, training_status.recall, training_status.accuracy_score, training_status.id)
    query = 'update training_status set `status` = %s, f1_score = %s, `precision` = %s, recall = %s, accuracy_score = %s, cross_val_score_mean=%s where id = %s'
    connection = getConnection()
    cursor = connection.cursor()
    update_tuple = (training_status.status, training_status.f1_score, training_status.precision, training_status.recall, training_status.accuracy_score, training_status.cross_val_score_mean, training_status.id,)
    cursor.execute(query, update_tuple)
    id_training_status = cursor.lastrowid
    connection.commit()
    cursor.close()
    connection.close()
    return True


def get_training_status_by_id_dataset(id_dataset):
    query = """select * from training_status where id_dataset = %s order by id"""
    connection = getConnection()
    cursor = connection.cursor()
    cursor.execute(query, (id_dataset,))
    tupla = cursor.fetchall()
    connection.commit()
    cursor.close()
    connection.close()
    return tupla


def get_training_status_by_id(id_training):
    query = """select * from training_status where id = %s"""
    connection = getConnection()
    cursor = connection.cursor()
    cursor.execute(query, (id_training,))
    tupla = cursor.fetchone()
    connection.commit()
    cursor.close()
    connection.close()
    return tupla
