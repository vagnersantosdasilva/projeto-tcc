import string
import threading

from pandas import DataFrame
from sklearn.metrics import make_scorer, accuracy_score, precision_score, recall_score, f1_score
from sklearn.model_selection import train_test_split, cross_validate, LeaveOneOut, KFold
from sklearn.naive_bayes import MultinomialNB
from sklearn import tree, metrics, model_selection
import nltk
from sklearn.feature_extraction.text import TfidfTransformer, CountVectorizer, TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import VotingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
import unidecode
from werkzeug.utils import secure_filename
import repository
import utils
from models import Dataset, Error, Training, Classifier
import pandas as pd
import io
from sklearn.model_selection import cross_val_score
import numpy as np
nltk.download('rslp')


classifier = Classifier()
data_frame = DataFrame()

def filter_unidecode(lista_de_frases):
    palavras_sem_acentos = [unidecode.unidecode(texto) for (texto) in lista_de_frases]
    return palavras_sem_acentos


def remove_stop_words(texts):
    stemmer = nltk.RSLPStemmer()
    exclude = set(string.punctuation)
    descriptions = texts.description
    descriptions_unidecode_filtered = \
        filter_unidecode(descriptions)
    token_white_space = nltk.tokenize.WhitespaceTokenizer()
    stop_words = nltk.corpus.stopwords.words('portuguese')
    stop_words_unidecode_filtered = \
        filter_unidecode(stop_words)
    processed_sentence = list()
    for sentence in descriptions_unidecode_filtered:
        new_sentence = list()
        sentence_words = token_white_space.tokenize(sentence)
        for word in sentence_words:
            word = word.lower()
            if word not in stop_words_unidecode_filtered:
                word_ = ''.join(ch for ch in word if ch not in exclude)
                #if len(word_) >0 : word_ = stemmer.stem(word_)
                if len(word_) > 0: word_ = word_
                new_sentence.append(word_)
        processed_sentence.append(' '.join(new_sentence))

    texts['processed'] = processed_sentence
    print(texts['processed'])
    return texts, None


def format_dataframe(dataset):
    try:
        if (dataset):
            df = pd.DataFrame(dataset.file_content)
            csv = df.to_csv(sep=dataset.separator)
            lines = csv.split("\n")
            new_lines = list()
            for i in range(1, len(lines), 1):
                if len(lines[i]) > 0: new_lines.append(
                    lines[i].split("b'")[1].replace("\\n", '').replace("b\\", "").replace('"', "").replace("'", ""))

            names = new_lines[0].split(dataset.separator)
            content = new_lines[1:]
            c0, c1, c2, c3, c4 = [], [], [], [], []

            for lines in content:
                if len(lines) > 0:
                    if len(lines.split(dataset.separator)[0]) > 0:
                        c0.append(lines.split(dataset.separator)[0])
                    else:
                        c0.append(0)

                    if len(lines.split(dataset.separator)[1]) > 0:
                        c1.append(lines.split(dataset.separator)[1])
                    else:
                        c1.append(0)

            # d = {names[0]: c0, names[1]: c1, names[2]: c2, names[3]: c3, names[4]: c4}
            d = {names[0]: c0, names[1]: c1}
            df = pd.DataFrame(data=d)
            df['description'] = replace_utf8(df.description)
            return df, None
    except AttributeError:
        return Error('Erro ao tentar montar dataframe com dados de dataset. Verifique se o separador csv está correto',
                     500), None
    except IndexError:
        return Error(' Provável problema em separador', 500), None
    except Exception as error:
        return Error('format_data_frame:' + error.args, 500), None


def replace_utf8(list):
    result = []
    for phrase in list:
        result.append(phrase.replace("\\xc3\\xa7", "ç").replace("\\xc3\\xba", "ú").replace("\\xc3\\xa1", "á").replace(
            "\\xc3\\xaa",
            "ê").replace(
            "\\xc3\\xa3", "ã").replace("\\xc3\\xa9", "é").replace("\\xc3\\xb5", "õ").replace("\\xc3\\xad",
                                                                                             "í").replace(
            "\\xc3\\xa0", "à").replace("\\xc3\\xb3", "ó").replace("\\xc3\\x83", "Ã").replace("\\xc3\\xb4", "ô"))
    return result

def decide_training(clf,x,y):
    lines = x.shape[0];
    scoring = ['precision_macro', 'recall_macro', 'f1_macro', 'accuracy']
    cv = LeaveOneOut()
    #loo = LeaveOneOut()
    #for train, test in loo.split(x):
        #print("%s %s" % (train, test))
    #cv = KFold(n_splits=lines , shuffle=True ,random_state=45)

    if lines < 1000 :

        scores = cross_validate(clf, x, y, cv=10, scoring=scoring)
        precision = np.mean([100 * round(r, 4) for r in scores['test_precision_macro']])
        recall = np.mean([100 * round(r, 4) for r in scores['test_recall_macro']])
        accuracy = np.mean([100 * round(r, 4) for r in scores['test_accuracy']])
        f1 = np.mean([100 * round(r, 4) for r in scores['test_f1_macro']])
        #accuracy =  np.mean(cross_val_score(clf,x,y,cv=cv,  scoring="accuracy"))
        #precision = np.mean (cross_val_score(clf,x,y,cv=cv,  scoring='precision'))
        #recall = np.mean (cross_val_score(clf,x,y,cv=cv, scoring='recall'))
        #f1 = np.mean (cross_val_score(clf,x,y,cv=cv, scoring='f1'))
        return accuracy,precision,recall,f1
    else:
        scores = cross_validate(clf, x, y, cv=5, scoring=scoring)
        precision = np.mean([100 * round(r, 4) for r in scores['test_precision_macro']])
        recall = np.mean([100 * round(r, 4) for r in scores['test_recall_macro']])
        accuracy = np.mean([100 * round(r, 4) for r in scores['test_accuracy']])
        f1 = np.mean([100 * round(r, 4) for r in scores['test_f1_macro']])
        return accuracy, precision, recall, f1

def start_training(dataset, training_status):
    try:
        if (dataset):

            data_frame_, error_format = format_dataframe(dataset)
            new_content, error_stop_words = remove_stop_words(data_frame_)


            tf_idf_X = TfidfVectorizer().fit_transform(new_content.processed)
            x = tf_idf_X
            y = new_content.priority

            gnb_clf = MultinomialNB()
            svm_clf = SVC()
            knn_clf = KNeighborsClassifier(n_neighbors=3)
            tree_clf = tree.DecisionTreeClassifier()
            rnd_clf = RandomForestClassifier()
            vc1 = VotingClassifier(
                estimators=([
                    ('tree_clf', tree_clf),
                    ('gnb_clf', gnb_clf),
                    ('rnd_clf', rnd_clf),
                    ('svm_clf', svm_clf),
                    ('knn_clf', knn_clf)]
                ), voting='hard'
            )

            accuracy,precision,recall,f1 = decide_training(vc1, x, y)

            predictor = vc1.fit(x, y)

            training_status.f1_score = float(f1)
            training_status.accuracy_score = float(accuracy)
            training_status.precision = float(precision)
            training_status.recall = float(recall)
            training_status.status = 'COMPLETED'
            repository.update_training_status(training_status)

            classifier.predict = predictor
            classifier.data_frame = new_content



    except Exception as error:
        training_status.status = "STOP"
        repository.update_training_status(training_status)
        print(error.args)


def convert_metrics_percentage(test, predict, cross_val_score_mean):
    try:
        f1 = 100 * round(metrics.f1_score(test, predict, average='macro'), 4)
        accuracy = 100 * round(metrics.accuracy_score(test, predict), 4)
        precision_score = 100 * round(metrics.precision_score(test, predict, average='macro'), 4)
        recall = 100 * round(metrics.recall_score(test, predict, average='weighted'), 4)
        cross_val_score_mean_ = 100 * round(cross_val_score_mean, 4)
        return f1, accuracy, precision_score, recall, cross_val_score_mean_
    except Exception as error:
        raise Exception("Falha ao tentar converter métricas")

def create_training_status(id_dataset):
    dataset, error = get_dataset(id_dataset)
    if error: return None, Error('Não foi possível recuperar o dataset. Verifique se o id_dataset é correto', 400)

    list_train = list()
    list_tupla = repository.get_training_status_by_id_dataset(id_dataset)
    if list_tupla: list_train = [Training(tupla[0], tupla[1], tupla[2]) for tupla in list_tupla if
                                 tupla[2] == 'PROCESSING']
    if len(list_train) > 0: return None, Error('Já existe processo de treinamento em andamento para o dataset', 400)

    training_status = repository.save_training_status(id_dataset, 'PROCESSING')
    threading.Thread(target=start_training, args=(dataset, training_status,)).start()
    return training_status, error


def update_training_status(id_training, status):
    if (repository.update_training_status(id_training, status)):
        return True
    return False


def get_dataset(id_dataset):
    try:
        tuple_response = repository.get_dataset_by_id(id_dataset)
        id_dataset_response = tuple_response[0]
        csv = tuple_response[1]
        name_file = tuple_response[2]
        sep = tuple_response[3]
        data_string = csv.decode('UTF-8')
        data = io.StringIO(data_string)
        df = pd.read_csv(data, sep=sep)
        dataset = Dataset(name_file, df, sep, id_dataset_response)
        return dataset, None
    except Exception as error:
        return None, Error(error.args, 400)


def save_dataset(file, separator):
    # TODO: chamar validação de arquivo
    if file:
        filename = secure_filename(file.filename)
        df = pd.DataFrame(file)
        csv = df.to_csv(sep=separator, encoding="utf-8")
        id_dataset = repository.save_dataset(csv, file.filename, separator)
        return Dataset(filename, None, separator, id_dataset)


def get_training_status(id_training):
    try:
        tuple = repository.get_training_status_by_id(id_training)
        if tuple:
            return Training(tuple[1], tuple[2], tuple[0], tuple[3], tuple[4], tuple[5], tuple[6], tuple[7]), None
        return Training(None, None, None), None
    except Exception as error:
        return None, error.args


def classify(message):
    if classifier.predict:
        d = {'description': [message]}
        df = pd.DataFrame(data=d)
        docs_news , error = remove_stop_words(df)
        docs_news_ = docs_news.processed

        df2 = classifier.data_frame

        tf_idf_vectorizer = TfidfVectorizer()
        tf_idf_vectorizer.fit_transform(df2['processed'])
        new_text_vector = tf_idf_vectorizer.transform(docs_news_)

        predict = classifier.predict
        classe = predict.predict(new_text_vector)

        return classe
    else:
        print('nenhum classificador montado')

