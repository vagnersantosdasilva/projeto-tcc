import csv

import exceptions

def file_validation(file_path,separator):
    try:
        file = open(file_path,'r')
        reader = csv.reader(file,delimiter=separator)
        number_collums = len(next(reader))
        file.seek(0)
        for row in reader:
            # print(row)
            if len(row) != number_collums :
                file.close()
                return False
        file.close()
        return True
    except Exception as error:
        exceptions.handler_error(error)



