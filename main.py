import service
from flask import Flask, jsonify, request

app = Flask(__name__)
UPLOAD_FOLDER = 'datasets'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/training', methods=['post'])
def task():
    try:
        body = request.get_json()

        if body and body['id_dataset']:
            training_status, error_role = service.create_training_status(body['id_dataset'])
            if training_status:
                return jsonify({'id': training_status.id,
                                'status': training_status.status,
                                'id_dataset': training_status.id_dataset
                                }), 202
            if error_role:
                return jsonify({'error': error_role.message}), error_role.code

        else:
            return jsonify({'erro': 'Id de dataset é necessário para iniciar treinamento!'}), 400
    except Exception as error:
        return jsonify({'error': error.args}), 500


@app.route('/training/<int:id>', methods=['GET'])
def get_status(id):
    if id:
        training, error = service.get_training_status(id)
        if training.id_dataset:
            return jsonify({'id': training.id,
                            'status': training.status,
                            'id_dataset': training.id_dataset,
                            'metrics': {
                                'f1_score': training.f1_score,
                                'precision': training.precision,
                                'recall': training.recall,
                                'accuracy_score': training.accuracy_score,
                                'cross_val_score_mean':training.cross_val_score_mean
                            }
                            }), 200
        else:
            if error:
                return jsonify({'error': error}), 500
            else:
                return jsonify({}), 200
    else:
        return jsonify({'error': "Necessário informar id de treinamento!"
                        }), 400


@app.route('/dataset/<int:id>', methods=['DELETE'])
def delete_dataset(id):
    try:
        if id:
            response, error_role = service.delete_dataset(id)
            if response: return jsonify(), 200
            if error_role: return jsonify({'error': error_role.message}), error_role.code
        else:
            return jsonify({'error': "Parâmetro id não informado!"}), 400
    except Exception as error:
        return jsonify({'error': error.args}), 500


@app.route('/dataset', methods=['POST'])
def upload_dataset():
    try:
        separator = request.headers['separator']
        file = request.files['data_file']
        dataset = service.save_dataset(file, separator)
        return jsonify(
            {'name': dataset.name,
             'separator': dataset.separator,
             'id': dataset.id
             }), 201
    except Exception as error:
        return jsonify({'error': error.args}), 500


@app.route('/classifier/voting', methods=['get'])
def classifier():
    try:
        body = request.get_json()
        if body and body['message']:
            message = body['message']
            print(message)
            classe = service.classify(message)
            return jsonify({'class': classe[0]}), 200
    except Exception as error:
        return jsonify({'error': error.args}), 500


@app.errorhandler(404)
def handler_404_error(_error):
    return jsonify({'error': 'O recurso não foi encontrado'}), 404


@app.errorhandler(500)
def handler_500_error(_error):
    return jsonify({'error': 'Erro interno no servidor'}), 500


if __name__ == '__main__':
    app.run(debug=True)
